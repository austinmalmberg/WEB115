var greeting = document.getElementById('greeting');
var visitorName;

function fizzbuzz() {
    var fizz = prompt("Enter a number", 3);
    var buzz = prompt("Enter another number", 5);
    
    if(document.getElementById('fizzlist') === null) {
        
        // create ordered list
        $('#main-content').append('<ol id=\"fizzlist\"></ol>');
        
    } else {
        
        // clear list items
        $('#fizzlist').empty();
    }
    
    // populate with list items
    for(var i = 1; i <= 140; i++) { // begin from 1 to match list numbering
        let itemText = "";
        
        if(isEvenlyDivisible(i, fizz) && isEvenlyDivisible(i, buzz))
            itemText = "Work...HIKE!";
        else if(isEvenlyDivisible(i, fizz))
            itemText = "Work...";
        else if (isEvenlyDivisible(i, buzz))
            itemText = "HIKE!";
        else
            itemText += "daydream";
        
        $('#fizzlist').append('<li>'+itemText+'</li>');
    }
}

function isEvenlyDivisible(i, j) {
    return i % j === 0;
}

$('#name-form').submit(function(event) {
    visitorName = $(this).serializeArray().map(o => o.value);
    
    // update greeting
    greeting.innerHTML = "Welcome to The Weekend Camper, " + visitorName.join(" ") + "!";
    
    fizzbuzz();
    
    // prevents the page from refreshing
    event.preventDefault();
});
