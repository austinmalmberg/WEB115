function fizzbuzz(count, fizzMap, defaultWord) {
	
	addTagToPage('output', 'ol', 'fizzlist');
	
	for(let i = 1; i <= count; i++) {
		
		let nodeText = "";
		
		for(var entry in fizzMap) {
			
			if(fizzMap.hasOwnProperty(entry)) {
				
				if(isEvenlyDivisible(i, entry))
					nodeText += ` ${fizzMap[entry]}`;
			}
		}
		
		if(nodeText.length === 0) nodeText += defaultWord;
		
		addTagToPage('fizzlist', 'li', null, nodeText);
	}
}

function isEvenlyDivisible(i, j) {
	return i % j === 0;
}

function submitForm() {
	
	clearOutput();
	
	// get name
	const visitorNameAsArray = Array.prototype.slice.call(
		document.getElementsByClassName('name')).map(node => node.value);
	
	const visitorName = visitorNameAsArray.filter(name => name.length > 0).join(" ").trim();
	
	// update greeting
	document.getElementById('greeting').innerHTML = function(name) {
		
		if (name.length === 0)
			return `Welcome to The Weekend Camper!`;
		
		return `Welcome to The Weekend Camper, ${name}!`;
		
	} (visitorName);
	
	
	const count = document.getElementById('count').value;
	
	const numbers = Array.prototype.slice.call(
		document.getElementsByClassName('numbers')).map(e => e.value);
	
	const words = Array.prototype.slice.call(
		document.getElementsByClassName('words')).map(e => e.value);
	
	// zip related items then filter inputs without numbers
	const fizzList = numbers.map((n, i) => [n, words[i]]).filter(e => e[0]);
	
	// converts the list to a map
	const fizzMap = fizzList.reduce((map, item) => {
		map[item[0]] = item[1];
		return map;
	}, {});
	
	// error handling
	try {
		
		if(!count) throw 'You must specify how many items in the list.';
		if(count < 1) throw 'Number must be greater than 0.';
		
	} catch(err) {
		addTagToPage('output', 'p', 'error_msg', err);
		document.getElementById('error_msg').style.color = 'Red';
		
		return;
	}
	
	fizzbuzz(count, fizzMap, document.getElementById('defaultWord').value);
}

function clearOutput() {
	document.getElementById('output').innerHTML = "";
}

function addTagToPage(parentId, tag, childId, text) {
  let node = document.createTextNode(text || "");
  let element = document.createElement(tag);
  
  if(childId) element.setAttribute('id', childId);
  element.appendChild(node);
  
  document.getElementById(parentId).appendChild(element);
}
