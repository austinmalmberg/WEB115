const LIST_LENGTH = 140;

var visitorName = [];

function fizzbuzz() {
	
	// creates or clear fizzlist
	if(document.getElementById('fizzlist') === null) {
		$('#main-content').append('<ol id=\"fizzlist\"></ol>');	// create ordered list
	} else {
		$('#fizzlist').empty();	// clear list items
	}

	// populate with list items
	for(var i = 1; i <= LIST_LENGTH; i++) { // begin from 1 to match list numbering
		let itemText = "";

		if(i % 3 === 0) itemText += "Work...";
		if (i % 5 === 0) itemText += "HIKE!";
		
		if(!itemText) itemText = "daydream";

		$('#fizzlist').append(`<li>${itemText}</li>`);
	}
}

$('#name-form').submit(function(event) {
	
	visitorName = $(this).serializeArray().map(o => o.value);

	if(visitorName.join("").length > 0) {
		document.getElementById('greeting').innerHTML = `Welcome to The Weekend Camper, ${visitorName.join(" ")}!`;
	}

	fizzbuzz();

	// prevents the page from refreshing
	event.preventDefault();
});
