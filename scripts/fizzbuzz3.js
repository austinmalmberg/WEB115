const LIST_LENGTH = 140;

const fizz_num = 3;
const buzz_num = 5;
const bang_num = 7;

var visitorName = [];

function fizzbuzz() {
	
	// creates or clear fizzlist
	if(document.getElementById('fizzlist') === null) {
		$('#main-content').append('<ol id=\"fizzlist\"></ol>');	// create ordered list
	} else {
		$('#fizzlist').empty();	// clear list items
	}

	// populate with list items
	for(var i = 1; i <= LIST_LENGTH; i++) { // begin from 1 to match list numbering
		let itemText = "";

		if (isEvenlyDivisible(i, fizz_num)) itemText += "Work...";
		if (isEvenlyDivisible(i, buzz_num)) itemText += "HIKE!";
		if (isEvenlyDivisible(i, bang_num)) itemText += " Camp!";
		
		if(!itemText) itemText = "daydream";

		$('#fizzlist').append(`<li>${itemText}</li>`);
	}
}

function isEvenlyDivisible(i, j) {
	return i % j === 0;
}

$('#name-form').submit(function(event) {
	
	visitorName = $(this).serializeArray().map(o => o.value);

	if(visitorName.join("").length > 0) {
		document.getElementById('greeting').innerHTML = `Welcome to The Weekend Camper, ${visitorName.join(" ")}!`;
	}

	fizzbuzz();

	// prevents the page from refreshing
	event.preventDefault();
});
