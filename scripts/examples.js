const MY_NAME = "Austin Malmberg";
const MY_AGE = 30;

var name;
var age;

function examples1_10() {

  name = getName();
  if(!name) return;
  
  age = getAge();
  if(!age) return;

  // alert
  // math expressions
  // string concatenation
  alert("Hello, " + name + "! " + "Next year, you'll be " + ++age + ". In 5 years, you'll be " + (age + 4) + ".");
}

function examples11_20() {
  let alpha = 'abcdefghijklmnopqrstuvwxyz';
  let vowels = 'aeiou';
  
  // arrays
  let arr = [];
  
  for(let r = 0; r < alpha.length; r++) {
    
    arr[r] = [];
    
    // nested FOR loop
    for(let c = 0; c < alpha.length; c++) {
      
      let pos = (r + c) % alpha.length;
      let letter = alpha.charAt(pos);
      
      if(vowels.includes(letter)) {
        
        // changing case
        letter = letter.toUpperCase();
        
      } else if(letter === 'y') {
        
        // nested IF statement
        if (c % 2 === 0)
          letter = letter.toUpperCase();
        else
          letter = " ";
        
      }
      
      // adding elements to array
      arr[r].push(letter);
      
    }
    
    // removing, inserting elements into array
    arr[r].splice(0, 0, arr[r].pop());
  }
  
  addTagToOutput('pre', arr.reduce(
    (acc, row) => acc + row.join(' ') + '\n', []
  ));
}

function examples21_30() {
  
  // MY_NAME = "Austin Malmberg"  
  
  let space = MY_NAME.indexOf(" ");
  let firstName = MY_NAME.slice(0, space);
  
  addTagToOutput('pre',
                 `First Name: ${firstName}\n` +
                 `Length:     ${firstName.length}`);
  
  let lengthAsString = firstName.length.toString();
  let lengthAsNumber = Number(lengthAsString);
  
  addTagToOutput('pre',
                `length returns a ${typeof firstName.length}.\n` +
                `lengthAsString is a ${typeof lengthAsString}.\n` +
                `lengthAsNumber is a ${typeof lengthAsNumber}.`);
  
  addTagToOutput('pre',
                `PI rounded to 4 digits is ${Math.PI.toFixed(4)}.`)
                 
}

function examples31_40() {
  
  let months = ['January', 'February', 'March', 'April', 'May', 'June', 
                'July', 'August', 'Septempber', 'October', 'November', 'December'];
  
  let now = new Date();
  
  let time = now.toLocaleTimeString();
  let date = now.getDate();
  let month = months[now.getMonth()];
  let year = now.getFullYear();
  
  let dayOfTheWeek = function getDayOfWeek(day) {
  
    switch(day) {
      case 0:
        return 'Sunday';
      case 1:
        return 'Monday';
      case 2:
        return 'Tuesday';
      case 3:
        return 'Wednesday';
      case 4:
        return 'Thursday';
      case 5:
        return 'Friday';
      case 6:
        return 'Saturday';
      default:
        return null;  // never called
    }
    
  } (now.getDay());

  
  addTagToOutput('pre',
                `It is currently ${time} on ${dayOfTheWeek}, ${month} ${date}, ${year}.`);
}

function examples41_50() {

  let arr = [];
  let counter = 1;
  
  // adds 20 elements to array
  do {
    arr.push(counter);
  } while(counter++ < 20);
  
  addTagToOutput('pre', arr);
}

function examples51_60() {
  let interactiveElements = document.getElementById('ex-elements');
  let imageContainer = interactiveElements.getElementsByTagName('div');

  let catImage = imageContainer[0].firstElementChild;
  catImage.src = "images/swtl_51-60_after.png";
  
  let catParagraph = imageContainer[0].children[1];  
  catParagraph.innerHTML = "Colored";
  catParagraph.style.color = "Brown";
}

function examples61_70() {  
  // target list
  let exampleDiv = document.getElementById('ex-elements');
  let exampleList = exampleDiv.firstElementChild;
  
  // create a new list item
  let newListItem = document.createElement('li');
  let itemText = document.createTextNode("Middle Item");
  newListItem.appendChild(itemText);
  
  // add new list item to existing list
  exampleList.insertBefore(newListItem, exampleList.lastElementChild);
  
  // create a style object
  let styleObject = {
    'margin-left': "20px",
    'background-color': "darkgrey",
    color: "crimson",
    'list-style-type': "circle"
  };
  
  let styleAsString = function convertObjPropertiesToString(obj) {
    
    let styleString = "";
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        styleString += `${key}: ${obj[key]};`
      }
    }

    return styleString;
    
  }(styleObject);
  
  exampleList.setAttribute('style', styleAsString);
}

function examples71_80() {
  
  // webpage object containing html elements
  function SimpleWebpage(header, para) {
    this.h1 = header;
    this.p = para;
  }
  
  // create new SimpleWebpage object and calls constructor
  let content = new SimpleWebpage("Hello, World!", "This is a sample paragraph.");
  
  // converts content to HTML
  let contentAsHTML = function convertContentToHTML(obj) {
    
    let output = "";
    for(var key in obj) {
      if (obj.hasOwnProperty(key)) {
        output += `<${key}>${obj[key]}</${key}>`;
      }
    }
    
    return output;
    
  }(content);
  
  let newWindow = window.open('', '', 'width=300,height=200,left=100,top=50');
  newWindow.document.write(contentAsHTML);
}

function examples81_89() {
  // display errors in red
  document.getElementById('output').style.color = 'Red';
  
  let user = {};
  
  // try/catch
  try {
    
    user.name = document.getElementsByName('fname')[0].value;
    if(user.name.length === 0) { throw 'Name cannot be empty.'; }   // throwing exceptions
    
    
    user.email = document.getElementsByName('email')[0].value.trim();
    if(!isEmailAddress(user.email)) { throw 'Enter a valid email address.'; }
    
    function isEmailAddress(email) {
      // cannot include spaces
      if(email.indexOf(' ') !== -1) return false;
      
      let indexOfAt = email.indexOf('@');
      
      // must contain only 1 @ symbol
      if(indexOfAt === -1) return false;
      if(indexOfAt !== email.lastIndexOf('@')) return false;
      
      // get parts of email address
      let username = email.slice(0, indexOfAt);
      let domainName = email.slice(indexOfAt + 1, email.lastIndexOf('.'));
      let domain = email.slice(email.lastIndexOf('.') + 1, email.length);
      
      // test for invalid lengths
      if(username.length === 0) return false;
      if(domainName.length === 0) return false;
      if(domain.length < 2) return false;
      
      // test for invalid characters
      if(startsOrEndsWithPeriod(username)) return false;
      if(startsOrEndsWithPeriod(domainName)) return false;
      
      function startsOrEndsWithPeriod(str) {
        return str.charAt(0) === '.' ||
          str.charAt(str.length - 1) === '.';
      }
      
      // test that username does not start with a number
      return isNaN(username.charAt(0));
    }
    
    
    user.gender = document.querySelector('input[name="gender"]:checked').value;
    if(user.gender === null) { throw 'Select your gender.'; }
    
    
    let interestsAsNodeList = document.querySelectorAll('input[name="interests"]:checked');
    if(interestsAsNodeList.length === 0) {throw 'Select at least one interest.'; }
    
    user.interests = Array.prototype.slice.call(interestsAsNodeList).map(node => node.value);
    
  } catch(err) {
    addTagToOutput('p', err);
    return;
  }
  
  let newWindow = window.open('', '', 'width=300,height=200,left=100,top=50');
  
  // test for popup blocker
  if(newWindow === null) {
    addTagToOutput('pre', 'Please disable your popup blocker.');
    return;
  }
  
  let userAsHTML = function convertUserToHTML(obj) {
    
    let output = "";
    for(var key in obj) {
      if (obj.hasOwnProperty(key)) {
        output += `<p>${key}: ${obj[key]}</p>`;
      }
    }
    
    return output;
  } (user);
  
  newWindow.document.write(userAsHTML);
}



/* PROMPTS */

function getName() {
  let name = "";
  
  // prompt
  do {
    name = prompt("Who are you?", "Austin");
    
    if(name.length === 0) {
      alert("Surely you have a name!");
    }
    
  } while(name.length === 0);
  
  return name;
}

function getAge() {
  let age = 0;
  
  do {
    age = getNumberFromPrompt("How old are you, " + name + "?", 30, true);

    if(age === null) break;
    
    if(age <= 0) {
      alert("Age must be greater than 0.");
    }

  } while (age <= 0);
  
  return age;
}

function getNumberFromPrompt(msg, defVal, allowNull) {
  let n;

  do {

    // prompts
    n = prompt(msg, defVal);
    
    // comparison operators
    if(allowNull && n === null) break;

    if(isNaN(n)) {
      alert(n + " is not a number. Try again.");
    } else {
      n = Number(n);
    }

  } while(isNaN(n));

  return n;
}



/* HELPERS */

function addTagToPage(parentElement, tag, childId, text) {
  let node = document.createTextNode(text);
  let element = document.createElement(tag);
  
  element.setAttribute('id', childId);
  element.appendChild(node);
  
  document.getElementById(parentElement).appendChild(element);
}

function addTagToOutput(tag, text) {
  addTagToPage('output', tag, `output-${tag}`, text);
}

function clearOutput() {
  document.getElementById('output').innerHTML = "";
}

function toTitleCase(str) {  
  return str.charAt(0).toUpperCase() + str.slice(1);
}



// populates codeblock with the code that will run on button click
var btnFunctions = document.getElementById('btn-run').getAttribute('onclick');

var mainFunctionIndexStart = btnFunctions.indexOf('examples');
var mainFunctionIndexEnd = mainFunctionIndexStart + btnFunctions.slice(mainFunctionIndexStart).indexOf('(');

var mainFunction = btnFunctions.slice(mainFunctionIndexStart, mainFunctionIndexEnd);
var fn = window[mainFunction];

addTagToPage('codeblock', 'pre', 'example-code', fn.toString());
