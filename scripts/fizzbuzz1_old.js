var greeting = document.getElementById('greeting');
var visitorName;

function fizzbuzz() {
    if(document.getElementById('fizzlist') === null) {
        
        // create ordered list
        $('#main-content').append('<ol id=\"fizzlist\"></ol>');
        
    } else {
        
        // clear list items
        $('#fizzlist').empty();
    }
    
    // populate with list items
    for(var i = 1; i <= 140; i++) { // begin from 1 to match list numbering
        let itemText = "";
        
        if(i % 3 === 0 && i % 5 === 0)
            itemText = "Work...HIKE!";
        else if(i % 3 === 0)
            itemText = "Work...";
        else if (i % 5 == 0)
            itemText = "HIKE!";
        else
            itemText += "daydream";
        
        $('#fizzlist').append('<li>'+itemText+'</li>');
    }
}

$('#name-form').submit(function(event) {
    visitorName = $(this).serializeArray().map(o => o.value);
    
    // update greeting
    greeting.innerHTML = "Welcome to The Weekend Camper, " + visitorName.join(" ") + "!";
    
    fizzbuzz();
    
    // prevents the page from refreshing
    event.preventDefault();
});
