var visitorName = [];

function fizzbuzz() {
	
	let message = getMessagePrompt();
	let counter = getNumber(message, 125);

	// prommpt was cancelled
	if(counter === null) { return; }
	
	// creates or clear fizzlist
	if(document.getElementById('fizzlist') === null) {
		$('#main-content').append('<ol id=\"fizzlist\"></ol>');	// create ordered list
	} else {
		$('#fizzlist').empty();	// clear list items
	}

	// populate list with items
	for(var i = 1; i <= counter; i++) { // begin from 1 to match list numbering
		let itemText = i % 2 === 0 ? "even" : "odd";

		$('#fizzlist').append(`<li>Weekend Camper - the number is ${itemText}</li>`);
	}
}

function getMessagePrompt() {
	
	if(visitorName[0].length === 0) {
		return "How high do you want to count?";
	}

	return `How high do you want to count, ${visitorName[0]}?`;
}

function getNumber(message, defaultVal) {
	
	var n;
	
	while(isNaN(n) || n === undefined) {
		n = prompt(message, defaultVal);
	}

	return n;
}

$('#name-form').submit(function(event) {
	
	visitorName = $(this).serializeArray().map(o => o.value);

	if(visitorName.join("").length > 0) {
		document.getElementById('greeting').innerHTML = `Welcome to The Weekend Camper, ${visitorName.join(" ")}!`;
	}

	fizzbuzz();

	// prevents the page from refreshing
	event.preventDefault();
});
